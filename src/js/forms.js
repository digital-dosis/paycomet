'use strict';

var validateEmpty = function(val) {
  return val.length ? true : false;
};
var validateCif = function(cif) {
  var re = /([a-z]|[A-Z]|[0-9])[0-9]{7}([a-z]|[A-Z]|[0-9])/g;
  return re.test(cif);
};
var validateEmail = function(email) {
  var re = /\S+@\S+\.\S+/;
  return re.test(email);
};
var validatePhone = function(phone) {
  var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,6}$/im;
  return re.test(phone);
};
var validate = {
  empty: validateEmpty,
  email: validateEmail,
  cif: validateCif,
  phone: validatePhone
};

var validateForm = function(e) {
  var errors = [];
  var $fields = $(this).find('.js-inputs');

  $('.js-error').removeClass('is-active');

  $fields.each(function(i, el) {
    var $el = $(el);
    var $input = $el.find('input, textarea').eq(0);
    var checks = $input.data('validate').split(',');
    $el.removeClass('has-errors is-valid');
    checks.forEach(function(check) {
      switch (check) {
        case 'tos':
          if (!$input.prop('checked')) {
            errors.push(check);
            $el.removeClass('is-valid');
            $el.addClass('has-errors');
            $el.find('.js-error[data-for="tos"]').addClass('is-active');
          }
          break;
        default:
          if (!validate[check]($input.val())) {
            errors.push(check);
            $el.removeClass('is-valid');
            $el.addClass('has-errors');
            $input.siblings('.js-error[data-for="' + check + '"]').addClass('is-active');
          } else {
            $el.addClass('is-valid');
          }
        break;
      }
    });
  });

  if (errors.length) {
    e.preventDefault();
  }
};

jQuery(document).ready(function($) {
  $('.js-form').on('submit', validateForm);
});
