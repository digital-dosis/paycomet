'use strict';

var ease = CustomEase.create('custom', 'M0,0 C0.126,0.382 0.139,0.691 0.314,0.82 0.412,0.892 0.818,1 1,1');

var controller = new ScrollMagic.Controller();

const showMenu = function() {
  TweenMax.fromTo('.header.js-animate-el', 1.5, {opacity: 0}, {opacity: 1} );
  TweenMax.fromTo('.page-wrapper.js-animate-el', .5, {opacity: 0, y: -25}, {opacity: 1, y: 0} );
  TweenMax.fromTo('.navigation.js-animate-el', .6, {opacity: 0, y: -40}, {opacity: 1, y: 0} );
  TweenMax.staggerFromTo('.navigation-cta .js-animate-el', .3, {opacity: 0, y: -40}, {opacity: 1, y: 0}, 0.3 );
}

var animateHomeCover = function() {
  TweenMax.staggerFromTo('.cover--home__text .js-animate-el', 1.5, {opacity: 0, y: 50}, {opacity: 1, y: 0}, 0.3 );
  TweenMax.fromTo('.header.js-animate-el', 2.5, {opacity: 0}, {opacity: 1} );
  TweenMax.fromTo('.page-wrapper--home__header.js-animate-el', 2.5, {opacity: 0, y: -25}, {opacity: 1, y: 0} );
  TweenMax.fromTo('.navigation.js-animate-el', 1.6, {opacity: 0, y: -40}, {opacity: 1, y: 0} );
  TweenMax.staggerFromTo('.navigation-cta .js-animate-el', 1.3, {opacity: 0, y: -40}, {opacity: 1, y: 0}, 0.3 );
  TweenMax.fromTo('.cover--home__hero--left.js-animate-el', 2, {opacity: 0, x: 150, y: -150}, {opacity: 1, x: 0, y: 0} );
  TweenMax.fromTo('.cover--home__hero--right.js-animate-el', 2.5, {opacity: 0, x: 100, y: -100}, {opacity: 1, x: 0, y: 0} );
  TweenMax.fromTo('.cover--home__hero--center.js-animate-el', 1.2, {opacity: 0, x: 100, y: -100}, {opacity: 1, x: 0, y: 0, delay: .3} );
  TweenMax.fromTo('.cover--home__image.js-animate-el', 2, {opacity: 0, x: 280, y: -280}, {opacity: 1, x: 0, y: 0, ease: ease, delay: .6} );
  TweenMax.to('.home-bg', 2, {opacity: 1, delay: 1.2});
}

var animatePlataformaCover = function() {
  TweenMax.staggerFromTo('.cover--plataforma__text .js-animate-el', 1.5, {opacity: 0, y: 50}, {opacity: 1, y: 0}, 0.3 );
  TweenMax.fromTo('.header.js-animate-el', 2.5, {opacity: 0}, {opacity: 1} );
  TweenMax.fromTo('.page-wrapper.js-animate-el', .5, {opacity: 0, y: -25}, {opacity: 1, y: 0} );
  TweenMax.fromTo('.page-wrapper--plataforma__header.js-animate-el', .5, {opacity: 0, y: -45}, {opacity: 1, y: 0} );
  TweenMax.fromTo('.navigation.js-animate-el', 1.6, {opacity: 0, y: -40}, {opacity: 1, y: 0} );
  TweenMax.staggerFromTo('.navigation-cta .js-animate-el', 1.3, {opacity: 0, y: -40}, {opacity: 1, y: 0}, 0.3 );
  TweenMax.fromTo('.cover--plataforma__hero--left.js-animate-el', 2, {opacity: 0, x: 150, y: -150}, {opacity: 1, x: 0, y: 0} );
  TweenMax.fromTo('.cover--plataforma__hero--right.js-animate-el', 2.5, {opacity: 0, x: 100, y: -100}, {opacity: 1, x: 0, y: 0} );
  TweenMax.fromTo('.cover--plataforma__hero--center.js-animate-el', 1.2, {opacity: 0, x: 100, y: -100}, {opacity: 1, x: 0, y: 0, delay: .3} );
  TweenMax.fromTo('.cover--plataforma__image.js-animate-el', 2, {opacity: 0, x: 280, y: -280}, {opacity: 1, x: 0, y: 0, ease: ease, delay: .6} );
}

var animateCover = function() {
  TweenMax.staggerFromTo('.cover .js-animate-el', 1.5, {opacity: 0, y: 50}, {opacity: 1, y: 0}, 0.3 );
  TweenMax.fromTo('.header.js-animate-el', 2.5, {opacity: 0}, {opacity: 1} );
  TweenMax.fromTo('.page-wrapper.js-animate-el', .5, {opacity: 0, y: -25}, {opacity: 1, y: 0} );
  TweenMax.fromTo('.navigation.js-animate-el', 1.6, {opacity: 0, y: -40}, {opacity: 1, y: 0} );
  TweenMax.staggerFromTo('.navigation-cta .js-animate-el', 1.3, {opacity: 0, y: -40}, {opacity: 1, y: 0}, 0.3 );
  TweenMax.fromTo('.cover__image, .cover--solutions__image', 1.2, {y:-50, opacity:0, ease: ease}, {opacity: 1, y: 0, delay: .4});
  TweenMax.fromTo('.cover-solutions-icons', 1, {y:50, opacity:0, ease: ease}, {opacity: 1, y: 0, delay: 1.2});
  TweenMax.to('.cover__decorative', 1.5, {opacity:1, delay: 1.5, ease: ease});
}

var animateHomeSolutions = function () {
  var tweenHomeSolutionsTitle = TweenMax.from('.home-solutions__title', 1.2, {y:60, opacity: 0, ease: ease});
  var solutionsTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.home-solutions',
      triggerHook: 0.65,
    })
    .setTween(tweenHomeSolutionsTitle)
    .addTo(controller);

  var tweenHomeSolutionsItems = TweenMax.staggerFrom($('.home-solution'), 2, {y:80, opacity: 0, ease: ease}, 0.3);
  var solutionsItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.home-solutions',
      triggerHook: 0.5,
    })
    .setTween(tweenHomeSolutionsItems)
    .addTo(controller);
}

var animationFeatures = function() {

  var tweenDropdownImage = TweenMax.from('.dispositives', 1.4, {opacity: 0, ease: ease});
  var dropdownImageScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.features',
      triggerHook: 0.5,
    })
    .setTween(tweenDropdownImage)
    .addTo(controller);

  var tweenDropdownTitle = TweenMax.from('.features__info h2', .6, {y:30, opacity: 0, ease: ease});
  var dropdownTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.features',
      triggerHook: 0.45,
    })
    .setTween(tweenDropdownTitle)
    .addTo(controller);

  var tweenDropdownItem = TweenMax.staggerFrom($('.dropdowns__item'), .6, {y:30, opacity: 0, ease: ease}, 0.2);
  var dropdownScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.features',
    triggerHook: 0.3,
  })
  .setTween(tweenDropdownItem)
  .addTo(controller);
}

var animateDemos = function() {
  var tweenDemosTitle = TweenMax.from('.demos__title', .6, {y:30, opacity: 0, ease: ease});
  var demosTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.demos',
      triggerHook: 0.5,
    })
    .setTween(tweenDemosTitle)
    .addTo(controller);

  var tweenDemosItem = TweenMax.staggerFrom($('.demos-slide'), .6, {y:30, opacity: 0, ease: ease}, 0.2);
  var demosItemScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.demos',
    triggerHook: 0.4,
  })
  .setTween(tweenDemosItem)
  .addTo(controller);
}

var animateInfoDev = function() {
  var tweenInfoDevItems = TweenMax.staggerFrom($('.info-developers__title, .info-developers__text, .info-developers .cta'), .6, {y:30, opacity: 0, ease: ease}, 0.2);
  var infoDevItemsScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.info-developers__content',
    triggerHook: 0.6,
  })
  .setTween(tweenInfoDevItems)
  .addTo(controller);

  var tweenInfoDev = TweenMax.from('.info-developers__code', .6, {y:30, opacity: 0, ease: ease});
  var infoDevScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.info-developers__content',
      triggerHook: 0.5,
    })
    .setTween(tweenInfoDev)
    .addTo(controller);
}

var animationCards = function() {
  var tweenCardsTitle = TweenMax.from('.landing-cards .subtitle', 1.2, {y:60, opacity: 0, ease: ease});
  var cardsTitleScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.landing-cards',
    triggerHook: 0.6,
  })
  .setTween(tweenCardsTitle)
  .addTo(controller);

  var tweenCards = TweenMax.staggerFrom($('.cards__item'), 2, {y:80, opacity: 0, ease: ease}, 0.3);
  var cardsScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.landing-cards',
    triggerHook: 0.5,
  })
  .setTween(tweenCards)
  .addTo(controller);
}

var animateHomeRates = function() {
  var tweentitle = TweenMax.from('.landing-rates__title', 2, {y:60, opacity:0, ease: ease})
  var titleScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.home-rates__content',
    triggerHook: 0.55,
  })
  .setTween(tweentitle)
  .addTo(controller);

  var tweenClientsHome = TweenMax.staggerFrom('.home-rates__client', 2, {y:80, opacity:0, ease: ease, delay: 1}, 0.3);
  var clientsHomeScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.home-rates__content',
    triggerHook: 0.5,
  })
  .setTween(tweenClientsHome)
  .addTo(controller)
}

var animateHomeBusiness = function() {
  var tweenBusinessTitle = TweenMax.from('.home-business__title', 2, {y:60, opacity:0, ease: ease})
  var businessTitleScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.home-business',
    triggerHook: 0.7,
  })
  .setTween(tweenBusinessTitle)
  .addTo(controller);

  var tweenBusinessItems = TweenMax.staggerFrom('.image-card', 2, {y:40, opacity:0, ease: ease, delay: 1}, 0.2);
  var businessItemsScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.home-business',
    triggerHook: 0.7,
  })
  .setTween(tweenBusinessItems)
  .addTo(controller)
}

var coverEmpresa = function() {
  var tweenCoverEmpresa = TweenMax.staggerFrom('.cover--empresa__title, .cover--empresa__text', 2, {y:40, opacity:0, ease: ease}, 0.2);
  var coverEmpresaScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.cover--empresa',
    triggerHook: 0,
  })
  .setTween(tweenCoverEmpresa)
  .addTo(controller)
}

var animateAbout1 = function() {
    var tweenRowAbout = TweenMax.staggerFromTo('.about__row--one .js-animate-el', .8, {y:40, opacity:0, ease: ease},{y:0, opacity:1, delay: 1}, 0.3);
    var rowAboutScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.about__row--one',
      triggerHook: 0.7,
    })
    .setTween(tweenRowAbout)
    .addTo(controller);
}

var animateAbout2 = function() {
    var tweenRowAbout2 = TweenMax.staggerFromTo('.about__row--two .js-animate-el', .8, {y:40, opacity:0, ease: ease},{y:0, opacity:1}, 0.8);
    var rowAbout2Scene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.about__row--two',
      triggerHook: 0.65,
    })
    .setTween(tweenRowAbout2)
    .addTo(controller);
}

var animateLocations = function() {
    var tweenLocationsTitle = TweenMax.from('.locations__title', 2, {y:60, opacity:0, ease: ease})
    var locationsTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.locations',
      triggerHook: 0.6,
    })
    .setTween(tweenLocationsTitle)
    .addTo(controller);

    var tweenLocationsItems = TweenMax.staggerFrom('.locations__item', 2, {y:40, opacity:0, ease: ease, delay: 1}, 0.2);
    var locationsItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.locations',
      triggerHook: 0.5,
    })
    .setTween(tweenLocationsItems)
    .addTo(controller)
}

var animateForms = function() {
    var tweenForm = TweenMax.staggerFromTo('.cover .js-animate-el', 2, {y:40, opacity:0, ease: ease}, {y:0, opacity:1, ease: ease, delay: 1.2}, 0.2);
    var formScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.formsPage',
      triggerHook: 0,
    })
    .setTween(tweenForm)
    .addTo(controller)
}

var animateRates = function() {
  var tweenRatesTitle = TweenMax.from('.rates__title', 1, {y:40, opacity: 0, ease: ease});
  var RatesTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.rates',
      triggerHook: 0.5,
    })
    .setTween(tweenRatesTitle)
    .addTo(controller);

    var tweenRatesItems = TweenMax.staggerFrom($('.rate-card'), 1, {y:60, opacity: 0, ease: ease}, 0.15);
    var ratesItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.rates',
      triggerHook: 0.4,
    })
    .setTween(tweenRatesItems)
    .addTo(controller);
}

var animateModelos = function() {
  var tweenModelosTitle = TweenMax.from('.modelos__title', 1, {y:40, opacity: 0, ease: ease});
  var modelosTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.modelos',
      triggerHook: 0.5,
    })
    .setTween(tweenModelosTitle)
    .addTo(controller);

  var tweenModelos = TweenMax.from('.modelos__container', 1, {y:40, opacity: 0, ease: ease});
  var modelosScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.modelos',
      triggerHook: 0.4,
    })
    .setTween(tweenModelos)
    .addTo(controller);
}

var animateConectividad = function() {
  var tweenConectividadTitle = TweenMax.from('.conectividad__title', 1, {y:40, opacity: 0, ease: ease});
  var conectividadTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.conectividad',
      triggerHook: 0.75,
    })
    .setTween(tweenConectividadTitle)
    .addTo(controller);

    var tweenConectividadItems = TweenMax.staggerFrom($('.conectividad__item'), 1, {y:60, opacity: 0, ease: ease}, 0.15);
    var conectividadItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.conectividad',
      triggerHook: 0.6,
    })
    .setTween(tweenConectividadItems)
    .addTo(controller);
}

var animateSteps = function() {
  var tweenStepsTitle = TweenMax.from('.landing-steps__title', 1, {y:40, opacity: 0, ease: ease});
  var stepsTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.landing-steps',
      triggerHook: 0.7,
    })
    .setTween(tweenStepsTitle)
    .addTo(controller);

  var tweenStepsImage = TweenMax.from('.landing-step__image', 1, {y:40, opacity: 0, ease: ease});
  var stepsImageScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.landing-steps',
      triggerHook: 0.55,
    })
    .setTween(tweenStepsImage)
    .addTo(controller);

  var tweenStepsText = TweenMax.staggerFrom($('.landing-step__title, .landing-step__text'), 1, {y:60, opacity: 0, ease: ease}, 0.15);
  var stepsTextScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.landing-steps',
    triggerHook: 0.45,
  })
  .setTween(tweenStepsText)
  .addTo(controller);
}

var animateStepsHow = function() {
  var tweenStepsHowTitle = TweenMax.from('.steps-how__title', 1, {y:40, opacity: 0, ease: ease});
  var stepsHowTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.steps-how',
      triggerHook: 0.75,
    })
    .setTween(tweenStepsHowTitle)
    .addTo(controller);

  var tweenStepsHowItems = TweenMax.staggerFrom($('.steps-how__item'), 1, {y:60, opacity: 0, ease: ease}, 0.25, slideGoToEnd);
  var stepsHowItemsScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.steps-how',
    triggerHook: 0.6,
  })
  .setTween(tweenStepsHowItems)
  .addTo(controller);
}

var animateStepsModul = function() {
  var tweenStepsModulTitle = TweenMax.from('.steps__title', 1, {y:40, opacity: 0, ease: ease});
  var stepsModulTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.steps',
      triggerHook: 0.5,
    })
    .setTween(tweenStepsModulTitle)
    .addTo(controller);

  var tweenStepsModulItems = TweenMax.staggerFrom($('.step'), 1, {y:60, opacity: 0, ease: ease}, 0.15);
  var stepsModulItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.steps',
      triggerHook: 0.4,
    })
    .setTween(tweenStepsModulItems)
    .addTo(controller);
}

var animateContact = function() {
  var tweenContact = TweenMax.staggerFrom($('.landing-contact__element'), 2, {y:60, opacity: 0, ease: ease}, 0.3);
  var contactScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.landing-contact',
    triggerHook: 0.65,
  })
  .setTween(tweenContact)
  .addTo(controller);
}

var animatePlataformaIntro = function() {
  var tweenIntroTitle = TweenMax.staggerFrom($('.intro__header, .intro__title, .intro__text'), 2, {y:60, opacity: 0, ease: ease}, 0.3);
  var introTitleScene = new ScrollMagic.Scene({
    reverse: false,
    triggerElement: '.intro',
    triggerHook: 0.5,
  })
  .setTween(tweenIntroTitle)
  .addTo(controller);

  var lineAfterTitle = CSSRulePlugin.getRule('.intro--services:after');
  var tweenLineTitle = TweenMax.from(lineAfterTitle, 1, {cssRule:{y:60, opacity: 0, ease: ease}});
  var introLineTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.intro',
      triggerHook: 0.4,
    })
    .setTween(tweenLineTitle)
    .addTo(controller);
}

var animateDistributiva = function () {
  TweenMax.fromTo('.title', 1.6, {opacity: 0, y: 40}, {opacity: 1, y: 0, delay: .5} );
  TweenMax.staggerFrom($('.cards__item'), 2, {y:80, opacity: 0, ease: ease, delay: .5}, 0.3);
}

var animateCardsModul = function() {
  var tweenCardsModul = TweenMax.from('.cards .subtitle', 1.2, {y:60, opacity: 0, ease: ease});
  var cardsModulScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.cards',
      triggerHook: 0.65,
    })
    .setTween(tweenCardsModul)
    .addTo(controller);

  if($('.cards-solutions-container').length){
    var tweenCardsModulTitle = TweenMax.from('.cards-solutions-container__title', 1.2, {y:60, opacity: 0, ease: ease});
    var cardsModulTitleScene = new ScrollMagic.Scene({
        reverse: false,
        triggerElement: '.cards',
        triggerHook: 0.65,
      })
      .setTween(tweenCardsModulTitle)
      .addTo(controller);
    var tweenCardsModulItems = TweenMax.staggerFrom($('.cards__item'), 2, {y:80, opacity: 0, ease: ease}, 0.3);
    var cardsModulItemsItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.cards',
      triggerHook: 0.5,
    })
    .setTween(tweenCardsModulItems)
    .addTo(controller);
  }else if($('.cards--services-solutions').length){
    var tweenCardsServicesItems = TweenMax.staggerFrom($('.services-cards .cards__item'), 2, {y:80, opacity: 0, ease: ease}, 0.3);
    var cardsServicesItemsItemsScene = new ScrollMagic.Scene({
        reverse: false,
        triggerElement: '.services-cards',
        triggerHook: 0.5,
      })
      .setTween(tweenCardsServicesItems)
      .addTo(controller);

    var tweenCardsServices = TweenMax.from('.cards--services-solutions', 1.2, {y:60, opacity: 0, ease: ease});
    var cardsServicesScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.video__content',
      triggerHook: 0,
    })
    .setTween(tweenCardsServices)
    .addTo(controller);

    var tweenCardsServicesItems = TweenMax.staggerFrom($('.cards--services-solutions a'), 2, {y:80, opacity: 0, ease: ease}, 0.3);
    var cardsServicesItemsItemsScene = new ScrollMagic.Scene({
        reverse: false,
        triggerElement: '.cards--services-solutions',
        triggerHook: 0.5,
      })
      .setTween(tweenCardsServicesItems)
      .addTo(controller);
  }else{
    var tweenCardsModulItems = TweenMax.staggerFrom($('.cards__item'), 2, {y:80, opacity: 0, ease: ease}, 0.3);
    var cardsModulItemsItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.cards',
      triggerHook: 0.5,
    })
    .setTween(tweenCardsModulItems)
    .addTo(controller);
  }
}

var animateClients = function() {
  var tweenClientsTitle = TweenMax.from('.logos__title', 1.2, {y:60, opacity: 0, ease: ease});
  var clientsTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.logos',
      triggerHook: 0.65,
    })
    .setTween(tweenClientsTitle)
    .addTo(controller);

  var tweenClients = TweenMax.staggerFrom($('.logos__item'), 2, {y:80, opacity: 0, ease: ease}, 0.3);
  var clientsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.logos',
      triggerHook: 0.45,
    })
    .setTween(tweenClients)
    .addTo(controller);
}

var animateTestimonials = function() {
  var tweenTestimonials = TweenMax.from('.video__content', 1.2, {y:60, opacity: 0, ease: ease});
  var testimonialsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.testimonials',
      triggerHook: 0.5,
    })
    .setTween(tweenTestimonials)
    .addTo(controller);
  if($('.testimonials--services').length){
    var tweenTestimonialsServicesTitle = TweenMax.from('.video--services__title', 1.2, {y:60, opacity: 0, ease: ease});
    var testimonialsServicesTitleScene = new ScrollMagic.Scene({
        reverse: false,
        triggerElement: '.video__content',
        triggerHook: 0.5,
      })
      .setTween(tweenTestimonialsServices)
      .addTo(controller);

    var tweenTestimonialsServices = TweenMax.from('.swiper-container--testimonials-services', 1.2, {y:60, opacity: 0, ease: ease});
    var testimonialsServicesScene = new ScrollMagic.Scene({
        reverse: false,
        triggerElement: '.video__content',
        triggerHook: 0.4,
      })
      .setTween(tweenTestimonialsServices)
      .addTo(controller);
  }else if($('.testimonials--clients-about').length){
    var tweenTestimonialsClientsTitle = TweenMax.from('.video--clients-about__title', 1.2, {y:60, opacity: 0, ease: ease});
    var testimonialsClientsTitleScene = new ScrollMagic.Scene({
        reverse: false,
        triggerElement: '.video__content',
        triggerHook: 0.5,
      })
      .setTween(tweenTestimonialsClientsTitle)
      .addTo(controller);

    var tweenTestimonialsClients = TweenMax.staggerFrom($('.video--clients-about__client'), 2, {y:80, opacity: 0, ease: ease}, 0.2);
    var testimonialsClientsScene = new ScrollMagic.Scene({
        reverse: false,
        triggerElement: '.video__content',
        triggerHook: 0.4,
      })
      .setTween(tweenTestimonialsClients)
      .addTo(controller);
  }
}

var animateSolutions = function() {
  var tweenSolutionsTitle = TweenMax.from('.solutions .subtitle', 1.2, {y:60, opacity: 0, ease: ease});
  var solutionsTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.solutions',
      triggerHook: 0.65,
    })
    .setTween(tweenSolutionsTitle)
    .addTo(controller);

  var tweenSolutionsItems = TweenMax.staggerFrom($('.solution'), 2, {y:80, opacity: 0, ease: ease}, 0.3);
  var solutionsItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.solutions',
      triggerHook: 0.5,
    })
    .setTween(tweenSolutionsItems)
    .addTo(controller);
}

var animateRatesPage = function() {
  var tweenRatesPageTitle = TweenMax.from('.rates__title', 1.2, {y:60, opacity: 0, ease: ease, delay: .5});
  var ratesPageTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.rates',
      triggerHook: 0,
    })
    .setTween(tweenRatesPageTitle)
    .addTo(controller);

  var tweenRatesPageItems = TweenMax.staggerFrom($('.rate-card'), 2, {y:80, opacity: 0, ease: ease, delay: .8}, 0.3);
  var ratesPageItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.rates',
      triggerHook: 0,
    })
    .setTween(tweenRatesPageItems)
    .addTo(controller);
}

var animateBlog = function() {
  var tweenBlogTitle = TweenMax.from('.header-blog .title', 1, {y:60, opacity: 0, ease: ease});
  var blogTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.header-blog',
      triggerHook: 0.3,
    })
    .setTween(tweenBlogTitle)
    .addTo(controller);

  var tweenBlogSearch = TweenMax.from('.header-blog .topmain', .4, {y:60, opacity: 0, ease: ease, delay: 1});
  var blogSearchScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.header-blog',
      triggerHook: 0.3,
    })
    .setTween(tweenBlogSearch)
    .addTo(controller);

  var tweenBlogItems = TweenMax.staggerFrom($('.card'), 2, {y:80, opacity: 0, ease: ease, delay: 1.2}, 0.3);
  var blogItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.header-blog',
      triggerHook: 0.3,
    })
    .setTween(tweenBlogItems)
    .addTo(controller);
}

var animateNewsletter = function() {
  var tweenNewsletter = TweenMax.staggerFromTo($('.newsletter .js-animate-el'), 2, {y:80, opacity: 0, ease: ease}, {y:0, opacity: 1, ease: ease}, 0.2);
  var newsletterScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.newsletter',
      triggerHook: 0.6,
    })
    .setTween(tweenNewsletter)
    .addTo(controller);
}

var animatePost = function() {
  var tweenArticleTitle = TweenMax.fromTo($('.post__wrapper'), 1.5, {y:80, opacity: 0, ease: ease}, {y:0, opacity: 1, ease: ease, delay: .4});
  var articleTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.post__header',
      triggerHook: 0,
    })
    .setTween(tweenArticleTitle)
    .addTo(controller);

  var tweenArticle = TweenMax.fromTo($('.post__article-wrapper'), 1.5, {y:80, opacity: 0, ease: ease}, {y:0, opacity: 1, ease: ease, delay: 1});
  var articleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.post__article',
      triggerHook: 0.7,
    })
    .setTween(tweenArticle)
    .addTo(controller);
}

var animateRelatedPosts = function() {
  var tweenRelatedTitle = TweenMax.fromTo($('.subtitle.post__related'), 1, {y:80, opacity: 0, ease: ease}, {y:0, opacity: 1, ease: ease});
  var relatedTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.gridblog',
      triggerHook: 0.7,
    })
    .setTween(tweenRelatedTitle)
    .addTo(controller);

  var tweenRelatedItems = TweenMax.staggerFromTo($('.gridblog .card'), 1.5, {y:80, opacity: 0, ease: ease}, {y:0, opacity: 1, ease: ease}, 0.2);
  var relatedItemsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.gridblog',
      triggerHook: 0.55,
    })
    .setTween(tweenRelatedItems)
    .addTo(controller);
}

var animateDevelopers = function() {
  var tweenDevelopersMenu = TweenMax.staggerFromTo($('.developers__menu > .developers__menu-item'), 1.5, {y:20, opacity: 0, ease: ease}, {y:0, opacity: 1, ease: ease, delay: .5}, 0.2);
  var developersMenuScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.developers',
      triggerHook: 0,
    })
    .setTween(tweenDevelopersMenu)
    .addTo(controller);

  var tweenDevelopers = TweenMax.staggerFromTo($('.developers__textBlock'), 1.5, {y:60, opacity: 0, ease: ease}, {y:0, opacity: 1, ease: ease, delay: 1}, 0.4);
  var developersScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.developers',
      triggerHook: 0,
    })
    .setTween(tweenDevelopers)
    .addTo(controller);
}

var animateDemosPage = function() {
  var tweenDemosPageTitle = TweenMax.fromTo($('.demos--page__title'), 1, {y:60, opacity: 0, ease: ease}, {y:0, opacity: 1, ease: ease, delay: .5});
  var demosPageTitleScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.demos',
      triggerHook: 0,
    })
    .setTween(tweenDemosPageTitle)
    .addTo(controller);

  var tweenDemosPanels = TweenMax.staggerFrom($('.demos-slide'), 2, {y:60, opacity: 0, ease: ease, delay: 1}, 0.3);
  var demosPanelsScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.demos',
      triggerHook: 0,
    })
    .setTween(tweenDemosPanels)
    .addTo(controller);

  var tweenDemosContent = TweenMax.from($('.demos--page__content'), 2, {y:60, opacity: 0, ease: ease, delay: 1.8});
  var demosContentScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.demos',
      triggerHook: 0,
    })
    .setTween(tweenDemosContent)
    .addTo(controller);
}

var slideGoToEnd = function(){
  if(mediaQuery('desktop') && $('.page-wrapper--solutions-page--05').length){
    swiperStepsHow.slideTo(2, 400);
  }
}

var showContentPage = function() {
  var tweenPageContent = TweenMax.from($('.contentPage'), 2, {y:60, opacity: 0, ease: ease, delay: 1});
  var pageContentScene = new ScrollMagic.Scene({
      reverse: false,
      triggerElement: '.page',
      triggerHook: 0,
    })
    .setTween(tweenPageContent)
    .addTo(controller);

  if($('.thankyou').length || $('.legal').length){
    var tweenPageContentThankyou = TweenMax.staggerFromTo($('.contentPage .js-animate-el'), 2, {y:60, opacity: 0, ease: ease}, {y:0, opacity: 1, ease: ease, delay: .5}, 0.4);
    var pageContentThankyouScene = new ScrollMagic.Scene({
        reverse: false,
        triggerElement: '.page',
        triggerHook: 0,
      })
      .setTween(tweenPageContentThankyou)
      .addTo(controller);
  }
}

var loadedPage = function() {
  setTimeout( function(){
    TweenMax.to('.loader', .2, {opacity: 0} );
    TweenMax.to('.loader', 0, {visibility: 'hidden', delay:.2, onComplete:init} );
  }, 1500);
}

var getPageName = function(){
  let pageType = ['home', 'distributive', 'plataforma', 'landing', 'empresa', 'page', 'formsPage', 'demosPage', 'ratesPage', 'blogHome', 'blogPost', 'desarrolladores' ];
  for ( var i = 0; i < pageType.length; i++ ){
    if ( $('.page-wrapper').hasClass( pageType[i] ) ){
      var  page = pageType[i];
      break;  
    }
  }
  return page;
}

var init = function() {
  if (mediaQuery('desktop')) {
    switch (getPageName()){
      case 'home':
        animateHomeCover();
        animateHomeSolutions();
        animationCards();
        animateHomeRates();
        animateHomeBusiness();
        animateContact();
        break;
      case 'distributive':
        showMenu();
        animateDistributiva();
        animateContact();
        break;
      case 'plataforma':
        animatePlataformaCover();
        animatePlataformaIntro();
        animateHomeBusiness();
        animateCardsModul();
        animateTestimonials();
        animateSolutions();
        animateHomeSolutions();
        animateContact();
        break;
      case 'landing':
        animateCover();
        animatePlataformaIntro();
        animationFeatures();
        animateStepsHow();
        animateConectividad();
        animateDemos();
        animateCardsModul();
        animateClients();
        animateTestimonials();
        animateInfoDev();
        animateModelos();
        animateRates();
        animateStepsModul();
        animateContact();
        break;
      case 'page':
        showMenu();
        showContentPage();
        break;
      case 'empresa':
        showMenu();
        coverEmpresa();
        animateAbout1();
        animateAbout2();
        animateLocations();
        animateTestimonials();
        animateContact();
        break;
      case 'desarrolladores':
        showMenu();
        animateDevelopers();
        break;
      case 'demosPage':
        showMenu();
        animateDemosPage();
        animateContact();
        break;
      case 'ratesPage':
        showMenu();
        animateRatesPage()
        break;
      case 'formsPage':
        showMenu();
        animateForms();
        break;
      case 'blogHome':
        showMenu();
        animateContact();
        animateBlog();
        animateNewsletter();
        break;
      case 'blogPost':
        showMenu();
        animatePost();
        animateRelatedPosts();
        animateNewsletter();
        break;
      default:
        showMenu();
        showContentPage();
    }
  }
}

jQuery(document).ready(function($) {
  loadedPage();
});
